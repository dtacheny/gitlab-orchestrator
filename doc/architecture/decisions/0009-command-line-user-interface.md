# Command Line User Interface

- Accepted: 2020-08-28

## Status

Accepted

## Context

- [Review Configure Discussion](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/281)
- [Review Other Options Discussion](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/282)

## Decision

1. Show command line help information
    ```
    orchestrator help
    ```
1. Ensure dependencies are installed
    ```
    orchestrator init
    ```
1. Diagnose usability of local Ansible and Terraform installation
    ```
    orchestrator doctor
    ```
    > **NOTE:** Decision to support doctor deferred to **Beta** or **GA** release.
1. Manage orchestrator and deployment configuration
    ```
    orchestrator set [SECRET_NAME] [SECRET_VALUE]
    orchestrator set backend [gcs|gitlab]
    orchestrator set key_pair PRIVATE_KEY PUBLIC_KEY
    orchestrator set provider_credential SITE_NAME CREDENTIAL_FILE
    orchestrator get VALUE
    ```
    > **NOTE:** The command line interface will detect the key name and
    > determine which configuration file/vault stores the information.
    > Changes merged to configuration file or vault.
1. Validate Orchestrator Settings
    - Check that all configuration is valid, secrets can be accessed, etc
    ```
    orchestrator validate config
    ```
    - Post deployment validation
    ```
    orchestrator validate deployment
    ```
    > **NOTE:** Implementation scheduled for **Beta**
1. Apply some or all of the **Orchestrator** modules
    ```
    orchestrator apply [all|provision|assemble|orchestrate]
    ```
1. Debugging runtime
    ```
    orchestrator --verbosity [silent|debug|default|all]
    ```

## Consequences

1. Simplifies binding configuration data into templates.
1. Simplifies configuration validation and testing.
1. Adds a built artifact to each release cycle.
1. Simplified commands are easier to learn and follow

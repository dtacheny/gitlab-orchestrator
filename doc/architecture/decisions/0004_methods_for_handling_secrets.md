# Methods for Handling Secrets

- Proposed: 2020-07-29
- Accepted: 2020-07-31

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/205)

## Decision

1. Orchestrator uses wrapper code to access secrets.
    - First iteration wraps around a custom software module that encrypts
      secrets to a file on disk.
    - Product will identify and prioritize integration with other vendors
      such as Hashicorp Vault in future iterations.
1. Orchestrator separates configuration and secrets.
    - Anyone may safely view configuration data.
    - Secret access limited to privileged administrators.
    - Secrets are kept on a separate mount or external service from the
      Orchestrator tooling.
    - Encrypt all secrets at rest.
    - Encrypt all secrets during transmission.
1. Orchestrator classifies two types of secret:
    - Keys required to exist on disk unencrypted during runtime.
        > **Example:** SSH private keys while Orchestrate module runs.
    - Passwords, tokens, and similar secrets are encrypted until use.
1. Orchestrator provides clear feedback when user action is required to
   retain dynamically generated secrets.

## Consequences

1. Creating a wrapper increases development time at the benefit of providing
   flexibility to integrate other secrets management solutions easily later.
1. Increased security at the expense of more complexity managing
   configuration locations. Mitigated through providing a command line
   interface to help abstract away the complexity.

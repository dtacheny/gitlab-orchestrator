# Command Line User Interface

- Proposed: 2020-07-31
- Accepted: 2020-08-07
- Superseded: 2020-08-28

## Status

Superseded by [ADR-0009](0009-command-line-user-interface.md)

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/242)

## Decision

1. Show command line help information
    ```
    orchestrator help
    ```
1. Install dependencies on first run
    ```
    orchestrator init
    ```
1. Update dependencies over tool lifetime
    ```
    orchestrator update
    ```
1. Manage Orchestrator tool configuration
    ```
    orchestrator configure [OPTIONS]
    ```
1. Diagnose broken Orchestrator tool configuration
    ```
    orchestrator doctor
    ```
1. Manage secrets for deployments
    ```
    orchestrator secrets set [SECRET_NAME] [SECRET_VALUE]
    orchestrator secrets add key_pair PRIVATE_KEY PUBLIC_KEY
    orchestrator secrets add provider_credential SITE_NAME CREDENTIAL_FILE
    orchestrator secrets export [--unmask]
    ```
1. Validate Orchestrator Settings
    - Deployment pre-flight checks
    ```
    orchestrator validate preflight
    ```
    - Post deployment validation
    ```
    orchestrator validate deployment
    ```
1. Apply the complete configuration including **Provision** and **Orchestrate** modules
    ```
    orchestrator apply
    ```
1. Perform a dry-run test showing what the **Provision** module would do and validating the user's configuration for **Orchestrate**
    ```sh
    orchestrator check
    ```
1. Provision resources only
    ```
    orchestrator provision
    ```
1. Assemble the inventory only
    ```
    orchestrator assemble
    ```
1. Configure the deployment
    ```
    orchestrator orchestrate
    ```
1. Debugging runtime
    ```
    orchestrator --verbosity [silent|debug|default|all]
    ```

## Consequences

1. Simplifies binding configuration data into templates.
1. Simplifies configuration validation and testing.
1. Adds a built artifact to each release cycle.

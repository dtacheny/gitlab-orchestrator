# Ansible Module and Plugin Plan

- Proposed: 2020-08-07
- Accepted: 2020-08-11

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/247)

## Decision

1. **Orchestrator** defines **Ansible** modules whenever it interacts with
   and parses output from `gitlab-ctl`.
1. **Orchestrator** defines **Ansible** plugins whenever **Jinja** or
   **Python** filters are difficult to maintain in playbooks or roles.
1. Group all GitLab modules as `gitlab_X` [per Ansible conventions](https://docs.ansible.com/ansible/latest/dev_guide/developing_modules_in_groups.html#developing-modules-in-groups).
1. [Ansible modules will be written in Python per Ansible Standards](https://docs.ansible.com/ansible/latest/dev_guide/developing_python_3.html#developing-python-3).
1. GitLab modules for Ansible will live in a separate repository and be
   installed to Orchestrator artifacts during the Orchestrator build and
   release process.

## Consequences

1. Orchestrator will not be dependent on the **command** and **shell**
   modules from Ansible which are not idempotent.
1. Allows cleaner Orchestrator code by allowing related data to be collated
   and handled in a more structured manner.

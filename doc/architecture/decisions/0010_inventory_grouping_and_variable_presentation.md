# Reading User Configuration

- Proposed: 2020-09-17
- Accepted: 2020-09-17

## Status

Accepted

## Context

- [Review Discussion](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/289)

## Decision

1. The `assemble-inventory` shell script creates unexpected behaviors.
    - Early adopters find `assemble-inventory` to be a pain point. They
      expect configuration changes to reflect immediately into
      `run-orchestration` and do not anticipate having to run
      `assemble-inventory` again.
1. Split `assemble-inventory` into components and graft them onto the other
   modules in alignment with user expectations.
1. Automatic randomized secret generation will move into the **Go** command
   line as a functional check before Orchestrate module invocation.
1. Secret ingestion will move into a custom Ansible inventory.
    1. First iteration will read from an encrypted **JSON** file or the
       shell environment.
    1. Future iterations will add modules supporting integrated vaults.
1. Node grouping will move from the **GCP_COMPUTE** module to a custom
   Ansible inventory using machine tags.
1. Secret ingestion and node grouping will be separate Ansible modules.
    1. Allows users to overide the node grouping for environments that opt
       against using the **Provision** module.
    1. Allows users to override the secrets ingestion with a custom module
       pointing at their own internal solution if our provided vaults are
       not compliant with their environment.

## Consequences

1. Simplifies the user experience.
1. Adds more modularity to support a broader variety of customer settings at
   the expense of simplicity.
1. Introduces Python as a supported language due to use of Ansible custom
   modules.
1. Enables iteration toward support for more Cloud Providers by eliminating
   the dependency on the **GCP_COMPUTE** module.

# 0002. User Experience Targets: Alpha Release

- Proposed: 2020-02-28
- Accepted: 2020-07-08

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/31)

## Decision

1. Command line interface to be written in **Go** per #29
    - Follow the pattern established by other communities and tools with widespread adoption.
    - GitLab has more maintainers to support **Go** than the alternatives.
    - **Go** allows us a more easily tested and maintainable way to deal with complex data.
1. Command line interface stands alone and is pre-compiled
    - Download and go provides users with simplicity.
1. Dependencies are packaged inside a standard container
    - Users should not be required to understand the complexities of managing other toolchains that may be used by **Orchestrator**.
1. Command line interface abstracts the container invocations from the end user
    - Users should not have to learn about the container's contents to use the tool effectively.
1. Initial Installation and User Upgrade interface is the same
    - The user should not have to learn alternative patterns for installation versus upgrades.
1. Graphical Interface is out of scope at this time
    - The minimum viable product our customers ask for is a command line interface. We can easily add a graphical interface later.
1. [Secrets management will be handled via its own discussion due to the topic's complexity](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/205).
    - The command line interface design can proceed on other topics until secrets management is firm.

## Consequences

- Benefits
    1. Input validation and testing improves immensely.
    1. Templates and data bindings become less onerous to maintain.
    1. Structured configuration files become easier to maintain.
- Drawbacks
    1. Converting to **Go** requires a full refactor of tooling. Mitigated
       by being projected to happen just once during the project lifecycle.
    1. Go is not as accessible to some users. Mitigate by improving logging
       facilities in the tooling.

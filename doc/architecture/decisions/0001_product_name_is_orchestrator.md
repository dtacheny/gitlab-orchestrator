# 0001. Product Name is Orchestrator

- Proposed: 2019-09-19
- Accepted: 2020-07-10

## Status

Accepted

## Context

[Review the original discussion issue for full context](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/159)

1. **Orchestrator** really is a meta orchestration tool; it orchestrates orchestration tools
1. The few alternate suggestions made did not garner upvotes/traction beyond simply being an alternative
1. Difficulty coming up with alternate names conveying the tool's purpose demonstrates that **Orchestrator** is likely the [most boring solution](https://about.gitlab.com/handbook/values/#boring-solutions)
1. If the name becomes a serious problem, we can [follow the two way door principle](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions)

## Decision

Orchestrator shall be retained as the project name.

## Consequences

1. Orchestrator may be confused with other tools that do more generic
   orchestration tasks.

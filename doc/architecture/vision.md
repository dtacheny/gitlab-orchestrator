---

# Architectural Vision

## Overview

The orchestrator is purposefully modular to allow as much flexibility as
possible. `Orchestrate` is the only required module because it is the policy
engine that applies a reference architecture configuration.

## Replacing the Provision Module

The provision module creates and tags nodes ready to be configured into a
GitLab site. Customers opting to replace this module should consider the
reference architecture examples and create that number of nodes.


## Replacing the Assembly Module

Customers who replace the provision module will also likely wish to replace
the assembly module. The assembly module is, at heart, a custom Ansible
inventory. It must return a defined set of groups and variables to interface
with the `Orchestrate` module.

## Orchestrator Sequence Diagram

This is a high level view of the **Orchestrator** process.

```mermaid
sequenceDiagram
    participant Human
    participant Provision
    participant Secrets Storage
    participant Assembly
    participant Orchestrate
    note right of Human: Request Node Creation
    Human->>Provision: Deploy nodes?
    alt GitLab Provided
        Provision->>Secrets Storage: Topology Output
    else Customer Provided
        Provision->>Human: Node Data
        Human->>Secrets Storage: Topology Output
    end
    note right of Human: Apply Configuration
    Human->>Orchestrate: Attempt Configuration
    Orchestrate->>Assembly: Request Site Inventory?
    alt GitLab Provided
        Assembly->>Secrets Storage: Build inventory
        Secrets Storage->>Assembly: Return inventory mechanism
        Assembly->>Orchestrate: Return inventory
    else Customer Provided
        Assembly->>Assembly: Build inventory
        Assembly->>Orchestrate: Return inventory
    end
    Orchestrate->>Orchestrate: Configure site(s)
    Orchestrate->>Human: Report Status
```

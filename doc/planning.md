# Orchestrator Planning

## Planning Sync Meetings

Maintainers and Product Management  meet once monthly to discuss project direction
and milestone planning.

## Issue Weights

The **Orchestrator** project is going to use **weights** to help determine
the load of various tasks.

### Base Weight

- count the number of people expected to be involved
- add challenge quantifiers

    |Quantifier|Weight|
    |-|-|
    | Database is involved | 4 |
    | Network Clustering is involved | 4 |
    | Missing Feature in GitLab Utilities | 12 |
    | Requires Custom Ansible module work | 36 |
    | Unclear or Partial Documentation Only | 12 |
    | Performance (speed, refactors) | 12 |
    | No Known Definition of Done | 12 |
    | Decisions Required | 4 |

### Calculating Total Weight

An issue weighs `base_weight * total_teams_involved`. This helps account for
complexity involved with collaboration across teams and time zones.

### Examples

- 1 Contributor, 1 Reviewer from the same team with no complications `(1 + 1) * 1 = 2`
- 1 Contributor, 1 Reviewer from the Same Team and a Decision is required `(1 + 1 + 4) * 1 = 6`
- 1 Contributor from three teams, 1 reviewer, and No Known Definition of Done `(1 + 1 + 1 + 1 + 12) * 3 = 48`

### Usage

Weight is currently arbitrary. We will track weight and see how it relates
to developer velocity and use that to adjust delivery expectations.

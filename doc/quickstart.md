# Orchestrator QuickStart Guide

[[_TOC_]]

## Software Requirements

**Orchestrator** installs all components required to run GitLab. Users must ensure the following software is present in their working environment prior to executing **Orchestrator** commands and tasks:

1. **Docker** or **Podman**
1. **bash**

## Cloud Provider Preparation

**Orchestrator** only provides support for **Google Cloud Platform** during
the **minimum viable product** and **Alpha** development phases.

1. [Learn about Google Cloud service accounts](https://cloud.google.com/iam/docs/understanding-service-accounts)
1. [Create a Service Account](https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account)
1. The Service Account should have the following permissions
   - `Storage Admin`
   - `Storage Object Admin`
   - `Compute Admin`
1. [Create a Service Account Key and download the **JSON** file to `secrets/`](https://cloud.google.com/iam/docs/creating-managing-service-account-keys)

**Orchestrator** keeps deployment information in object storage. Do not
manually edit this data after it is created.

1. [Go to Google Storage Browser](https://console.cloud.google.com/storage/browser)
1. [Create a bucket using any name](https://cloud.google.com/storage/docs/quickstart-console#create_a_bucket) it will be the `STATE_GCS_BUCKET`
1. [Create a directory with any name inside the bucket](https://cloud.google.com/storage/docs/quickstart-console#create_folders) and it will be the `GCS_PREFIX`

## Set up the Orchestrator

### Clone the repository

Clone the `gitlab-orchestrator` repository to the machine where Orchestrator will run:

- If you are a user/customer that wants to use Orchestrator, clone via HTTP:

  ```sh
  git clone https://gitlab.com/gitlab-org/gitlab-orchestrator.git
  ```

- If you have write access to this repository and want to contribute to it, clone via SSH:

  ```sh
  git clone git@gitlab.com:gitlab-org/gitlab-orchestrator.git
  ```

**NOTE:** The Orchestrator is undergoing rapid development. To access the latest features, make sure you pull from master regularly.

## Configuration Options

**Orchestrator** looks for user configured secrets in the `secrets/` directory. Most secrets support automatic generation and persistence except for **SSH** keypairs as noted in the next section.

### SSH Key Management

**Orchestrator** uses **SSH** keypairs to securely connect to resources it
configures.

> **NOTE:** **SSH** keypairs generated automatically will not be persisted
> after the container exits.

- [Use a persistent **SSH** keypair](../scripts/dev-container/README.md#using-persistent-ssh-keys)
- [Specify an alternate secrets directory](../scripts/dev-container/README.md#specifying-secrets-locations)

### Secrets Management

**Orchestrator** encrypts all data using **Ansible Vault**. The following
instructions detail how to set an **Ansible Vault** password and configure
**Orchestrator** to use the Cloud Provider credentials created in earlier
steps.

1. Create the text file `secrets/vault-password-file` and add a single-line,
   non-empty string. **Orchestrator** encrypts secrets passed to the
   deployment using this string as the password.
1. Copy `secrets/examples/gitlab-cluster-rc.sh.EXAMPLE` to `secrets/gitlab-cluster-rc.sh`
1. Edit `secrets/gitlab-cluster-rc.sh`
    - The example file only mentions required secrets
    - [Learn about the default options and others such as the `PACKAGE_*` variables which control the installed version of GitLab](../scripts/deploy/README.md#rc-file-options).
1. **Orchestrator** deploys 1 application node, 3 database nodes, and 3 consul nodes by default. Copy `secrets/examples.gitlab-cluster.X.json` to `secrets/gitlab-cluster.json` and [edit to override this behavior.](../scripts/deploy/README.md#cluster-topology-options)

## Run the Orchestrator

### Launch the runtime container

Execute and enter the runtime container
    ```sh
    ./scripts/dev-container/run-dev-container
    ```

### Execute Orchestrator inside the runtime container

1. Provision resources
    ```sh
    ./provision-cluster
    ```
1. Assemble inventory information
    ```sh
    ./assemble-inventory
    ```
1. Configure the Deployment
    ```sh
    ./run-orchestration
    ```
1. Get Password to log into web interface as user `root`
    ```sh
    ./decrypt-password
    ```

## Post Orchestration

The steps below should be executed inside the runtime container.

- Information about resource ip addresses is stored inside the container at
  `/repo/gitlab_provisioning_data.json`
- Connect to a resource machine
    ```sh
    ssh -i SSH_KEY_NAME ansible@IP_ADDRESS
    ```
- Tear down the entire deployment and remove cloud provider resources
    ```sh
    ./deprovision-cluster
    ```

## Further Reading

1. [Runtime Container Documentation](../scripts/dev-container/README.md)
1. [Deployment Module Documentation](../scripts/deploy/README.md)
1. [Configuration in Depth](../scripts/deploy/README.md#configuration-in-depth).
1. [Troubleshooting Guide](troubleshooting.md)

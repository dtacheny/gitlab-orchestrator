# Troubleshooting Orchestrator

The information below should help with most potential issues. If you need further help, please [open an issue](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues).

## Developer Container Issues

### Permissions Issues

- [Linux users may experience an error between runs of the container](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/199).

    ```
    error checking context: 'no permission to read from 'YOUR_PATH/gitlab-orchestrator/ansible/deployed_secrets/ssh/ssh_host_dsa_key''.
    ```
    > The `deployed_secrets` directory is created to help facilitate copies
    > between orchestrator runs. If you are trying to reconnect to an
    > existing cluster:
    >
    > 1. Move the directory to a location outside the repository
    > 1. Start the developer container
    > 1. Move the directory back and check access from inside the container

## Terraform Issues

### Backend initialization issues

- First step: ensure the configuration values are correct

  > The first troubleshooting step should be to validate the configuration
  > in your **gitlab-cluster-rc.sh** file. You can find more information on each
  > parameter by referencing the
  > [configuration in depth table](../scripts/deploy/README.md#configuration-in-depth).

- Terraform may respond that permission to the bucket is denied.

  ```
  Error: Failed to get existing workspaces: querying Cloud Storage failed: googleapi: Error 403: <service account> does not have storage.objects.list access to the Google Cloud Storage bucket., forbidden
  ```
  > If the service account is not created with the proper permissions,
  > Terraform will not be able to reach your backend storage location.
  > To address this, refer to the service account permissions list in the
  > [quickstart software requirements section](quickstart.md#software-requirements).

- [Terraform may be unable to initialize the backend](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/168).

  ```
  Error: Error inspecting states in the "gcs" backend: querying Cloud Storage failed: storage: bucket doesn't exist
  ```
  > If the configuration passed into **gitlab-cluster-rc.sh** was
  > incorrect and `provision-cluster` was executed, the incorrect
  > values can get stuck in the local **terraform.tfstate** file. For
  > example, if using GCS as the backend, the "bucket" and "prefix"
  > values in this file may still refer to the incorrect values.
  > To address this and proceed to backend initialization:
  >
  > 1. Ensure the configuration is correct in **the gitlab-cluster-rc.sh** file
  > 1. In the container, delete the `/repo/terraform/.terraform/terraform.tfstate` file
  > 1. Run the `provision-cluster` script again

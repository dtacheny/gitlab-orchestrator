# Project History

## The Initial Proposal

[The initial proposal for **Orchestrator** only meant to decouple the pieces
inside an existing continuous integration pipeline][proposal]. The eventual
idea was to move it toward something shareable, but that notion caught on
quickly and the **Orchestrator** project was born. The early discussions
took place between the Distribution Team, the Geo Team, Professional
Services, and Support.


## The First Step

The [architectural vision](architectural_vision.md) describes the high level
business logic behind the **Orchestrator**. It is too big to deliver all at
once, so we turned to [iteration][iteration_value]. The Geo Team was
selected as customer zero as they had a need for upgrade testing.

## Steady Steps and Hurdles

A lot of groundwork was required to lay out the **Orchestrator**.

1. [We started with a standard set of scripts to make pipeline and developer workflow aligned](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/18).
1. [We cut the original **Orchestrator** into its own project to increase
   velocity](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/commit/7d97675ed0d7ea0c32329a5a9d741f69d0cbcb93)
1. [We had to allow users to define site topology in an easy way](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/25)
1. [Geo Support required automatic license installation](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/176)
1. [Secure Initial Deployment with randomized GitLab root password is good too](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/173)

It took all of the above and a number of auxiliary issues and research just
to consider automating an install with **Geo** features.

## Immediate Benefits

There is a lot of documentation to read going from zero to nine nodes
configured with a **GitLab Geo** primary and secondary multi-node sites.
Running through it with fresh eyes provided some immediate value.

- [An installation `rake` task could return false negatives](https://gitlab.com/gitlab-org/gitlab/-/issues/219572)
- Documentation Improvements
    - [Using SSL Certificates with two Databases on the same node](https://gitlab.com/gitlab-org/gitlab/-/issues/220362)
    - [Setting the foreign data wrapper user password correctly](https://gitlab.com/gitlab-org/gitlab/-/issues/220360)

It's all little things; but little things add up and help improve everyone's
experience.


## The Future

The future isn't written yet, but there's an
[architectural roadmap](architectural_vision.md) that lights the way.

[proposal]: https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/3
[iteration_value]: https://about.gitlab.com/handbook/values/#iteration

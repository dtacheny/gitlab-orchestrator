output "hostnames" {
  value = module.sites.hostnames
}

output "external_addresses" {
  value = module.sites.external_addresses
}

output "internal_addresses" {
  value = module.sites.internal_addresses
}

output "deployment_sites" {
  value = var.deployment_sites
}

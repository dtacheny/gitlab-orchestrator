################################################################################
# Selector Variables
################################################################################
variable "os_name" {
  description = "the colloquial name of the operating system"
  type = string
  default = ""
}

variable "os_version" {
  description = "the version number of the operating system"
  type = string
  default = ""
}

variable "site_provider" {
  description = "the service providing site resources"
  type = string
  default = ""
}

################################################################################
# Supported Operating Systems Hash Map
################################################################################
locals {
  os_data = {
    "centos" = "${local.centos}",
    "debian" = "${local.debian}",
    "ubuntu" = "${local.ubuntu}"
  }
}

################################################################################
# Calculated Data
################################################################################
locals {
  distribution = local.os_data[local.os_name]
  distro_version = local.distribution[local.os_version]
  os_image = local.distro_version[local.site_provider]
}

locals {
  os_name = var.os_name == "" ? "ubuntu" : var.os_name
  os_version = var.os_version == "" ? "default" : var.os_version
  site_provider = var.site_provider == "" ? "google" : var.site_provider
}

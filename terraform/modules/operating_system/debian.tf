################################################################################
# Debian Cloud Provider Image Definitions
################################################################################
locals {
  debian = {
    "default" = {
      "google" = "debian-cloud/debian-10"
    },
    "10" = {
      "google" = "debian-cloud/debian-10"
    },
    "buster" = {
      "google" = "debian-cloud/debian-10"
    },
    "9" = {
      "google" = "debian-cloud/debian-9"
    },
    "stretch" = {
      "google" = "debian-cloud/debian-9"
    }
  }
}

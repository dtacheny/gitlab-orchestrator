################################################################################
# Ubuntu Cloud Provider Image Definitions
################################################################################
locals {
  ubuntu = {
    "default" = {
      "google" = "ubuntu-os-cloud/ubuntu-1804-lts"
    },
    "18" = {
      "google" = "ubuntu-os-cloud/ubuntu-1804-lts"
    },
    "bionic" = {
      "google" = "ubuntu-os-cloud/ubuntu-1804-lts"
    },
    "16" = {
      "google" = "ubuntu-os-cloud/ubuntu-1604-lts"
    },
    "xenial" = {
      "google" = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }
}

################################################################################
# Reference Architecture: 2k Users
# Module Inputs
################################################################################

variable "site_provider" {
  description = "The service that terraform will use to generate resources"
}

################################################################################
# Reference Architecture: 2k Users
# Module Definition
################################################################################

locals {
  layout = [
    {
      "resource_type"  = "application",
      "resource_count" = 2,
      "machine_size"   = {
        "google"       = "n1-highcpu-8"
      }
    },
    {
      "resource_type"  = "database",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-2"
      }
    },
    {
      "resource_type"  = "gitaly",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-4"
      }
    },
    {
      "resource_type"  = "load-balancer",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-highcpu-2"
      }
    },
    {
      "resource_type"  = "monitor",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-highcpu-2"
      }
    },
    {
      "resource_type"  = "redis",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-1"
      }
    },
  ]
}

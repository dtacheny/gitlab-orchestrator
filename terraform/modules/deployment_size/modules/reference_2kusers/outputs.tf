################################################################################
# Reference Architecture: 2k Users
# Module Outputs
################################################################################

output "resource_list" {
  description = "List of resources that make up a deployment"

  # iterate over each map entry describing resource type
  # select the right machine size for the site provider
  # inject site provider into each mapping
  value = [
    for entry in local.layout:
    merge(
      {
        for key, value in entry :
        key => key == "machine_size" ?
        value[var.site_provider] :
        try(tonumber(value), tostring(value))
      },
      { "site_provider" = var.site_provider }
    )
  ]
}

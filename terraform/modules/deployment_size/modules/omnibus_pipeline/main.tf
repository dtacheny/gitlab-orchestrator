################################################################################
# GitLab Continuous Integration: Omnibus Pipeline
# Module Definition
################################################################################

locals {
  layout = [
    {
      "resource_type"  = "application",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-1"
      }
    },
    {
      "resource_type"  = "database",
      "resource_count" = 3,
      "machine_size"   = {
        "google"       = "n1-standard-1"
      }
    },
    {
      "resource_type"  = "consul",
      "resource_count" = 3,
      "machine_size"   = {
        "google"       = "g1-small"
      }
    }
  ]
}

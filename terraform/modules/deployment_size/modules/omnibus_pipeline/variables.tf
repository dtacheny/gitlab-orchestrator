################################################################################
# GitLab Continuous Integration: Omnibus Pipeline
# Module Inputs
################################################################################

variable "site_provider" {
  description = "The service that terraform will use to generate resources"
}

################################################################################
# GitLab Continuous Integration: Geo Secondary Site
# Module Inputs
################################################################################

variable "site_provider" {
  description = "The service that terraform will use to generate resources"
}

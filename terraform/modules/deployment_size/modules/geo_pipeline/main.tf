################################################################################
# GitLab Continuous Integration: Geo Secondary Site
# Module Definition
################################################################################

locals {
  layout = [
    {
      "resource_type"  = "application",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-1"
      }
    },
    {
      "resource_type"  = "database",
      "resource_count" = 1,
      "machine_size"   = {
        "google"       = "n1-standard-1"
      }
    }
  ]
}

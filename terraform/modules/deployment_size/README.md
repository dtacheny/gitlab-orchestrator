# GitLab Deployment Size Module

## Purpose

This module creates a data structure describing the total number of
resources required to provision a GitLab Deployment.

## Interface Guarantees

1. No resources are created by this module
1. Outputs `deployment_resources` which is a list of maps
1. Each map in `deployment_resources`:
    - describes a single resource
    - identifies where the resource should be created
    - identifies the GitLab Deployment Object type name such as
      `application`, `database`, etc
    - provides a site name for use in Geo deployments

Here's an example map for an application resource:

```json
{
  "machine_size"   = "n1-highcpu-8"
  "resource_count" = 2
  "resource_type"  = "application"
  "site_name"      = "gitlab"
  "site_provider"  = "google"
}
```

## Invoking the Module

The deployment size module accepts two inputs:

1. `site_provider`
1. `deployment_sites`

### Site Provider

Set `site_provider` to provision resources on a specific cloud provider.
Google Compute is the default platform and the only one currently supported.

### Deployment Sites

Set `deployment_sites` to associate a site name with a deployment size
describing a collection of resources.

```terraform
deployment_sites = { site_name = deployment_size }
```

### Deployment Site Options

Set the `deployment_size` value to any of the following keys:

- `2k_users` defines the [GitLab 2,000 user reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html)
- `geo_pipeline` defines a single geo secondary site used in GitLab continuous integration pipelines
- `omnibus_pipeline` defines a seven node cluster with 1 application node, 3 database nodes, and 3 consul nodes as used in the GitLab continuous integration pipelines

Custom sizing is not supported at this time.

### Example Configurations

- Deploy a basic 2k reference architecture
    ```terraform
    deployment_sites = { "my_site" = "2k_users" }
    ```
- Deploy a primary GitLab site with a geo secondary site
    ```terraform
    deployment_sites = { "primary_site" = "omnibus_pipeline", "secondary_site" = "geo_pipeline" }
    ```

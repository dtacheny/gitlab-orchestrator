################################################################################
# Deployment Size
# Module Definition
################################################################################

# Calculate Returned Deployment Size
################################################################################

locals {
  # map site names to deployment size options
  site_deployment_sizes = try(
    tomap(var.deployment_sites),
    {}
  )

  # maps valid user input values for deployment_size to pre-defined
  # descriptions of resource collections
  resource_list_lookup_table = {
    "2k_users"               = module.reference_2kusers.resource_list
    "geo_pipeline"           = module.geo_pipeline.resource_list
    "omnibus_pipeline"       = module.omnibus_pipeline.resource_list
  }
}

# Modules Describing Pre-Made Deployment Sizes
################################################################################

module "reference_2kusers" {
  source        = "./modules/reference_2kusers"
  site_provider = var.site_provider
}

module "geo_pipeline" {
  source        = "./modules/geo_pipeline"
  site_provider = var.site_provider
}

module "omnibus_pipeline" {
  source        = "./modules/omnibus_pipeline"
  site_provider = var.site_provider
}

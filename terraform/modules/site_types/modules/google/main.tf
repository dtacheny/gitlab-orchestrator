resource "google_compute_instance" "google_cloud_vm" {
  ##############################################################################
  # Node Technical Specifications
  ##############################################################################
  count = length(var.deployment_resources)

  machine_type = element(var.deployment_resources, count.index)["machine_size"]

  boot_disk {
    initialize_params {
      image = var.os_image
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  ##############################################################################
  # Remote Management Information
  ##############################################################################
  metadata = {
    ssh-keys = "${var.ssh_user}:${var.ssh_public_key}"
  }

  ##############################################################################
  # Node Identifiers
  ##############################################################################
  name  = replace("${var.site_label}-${element(var.deployment_resources, count.index)["site_name"]}-${element(var.deployment_resources, count.index)["resource_type"]}-${element(var.deployment_resources, count.index)["resource_index"]}", "_", "-")

  tags  = compact(flatten(concat([
    var.site_label,
    element(var.deployment_resources, count.index)["resource_type"],
    element(var.deployment_resources, count.index)["site_name"],
    ], [element(var.deployment_resources, count.index)["resource_type"] == "database" ? (element(var.deployment_resources, count.index)["resource_index"] == 1 ? "primary" : "replica") : ""],
    [contains(["application", "monitor"], element(var.deployment_resources, count.index)["resource_type"]) ? ["http-server", "https-server"] : [""]])))

  labels = {
    ansible_group = "${var.site_label}-${element(var.deployment_resources, count.index)["resource_type"]}"
    cluster_group = "${var.site_label}-gcp-cluster"
  }
}

output "hostnames" {
  value = google_compute_instance.google_cloud_vm[*].name
  description = "Host names of google compute instances"
}

output "internal_addresses" {
  value = google_compute_instance.google_cloud_vm[*].network_interface[0].network_ip
}

output "external_addresses" {
  value = google_compute_instance.google_cloud_vm[*].network_interface[0].access_config[0].nat_ip
}

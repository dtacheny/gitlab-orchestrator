################################################################################
# Site Type Inputs
################################################################################

variable "deployment_resources" {
  description = "data structure describing all resources required to provision deployment"
  default     = {}
}

variable "prefix" {
  description = "cluster identifier creating namespace"
}

variable "os_image" {
  description = "The underlaying operating system that powers the node"
}

################################################################################
# Remote Management Information
################################################################################

variable "ssh_user" {
  description = "Remote access user account"
  default = "ansible"
}

variable "ssh_public_key" {
  description = "SSH public key for remote access"
}

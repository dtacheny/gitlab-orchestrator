module "google_module" {
  source               = "./modules/google/"
  deployment_resources = local.google_resources
  site_label           = var.prefix
  ssh_user             = var.ssh_user
  ssh_public_key       = var.ssh_public_key
  os_image             = var.os_image
}

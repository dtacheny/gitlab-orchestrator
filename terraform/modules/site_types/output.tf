################################################################################
# Site Type Outputs
################################################################################

output "deployment_resources" {
  value = var.deployment_resources
  description = "data structure describing all resources required to provision deployment"
}

output "google_resources" {
  value = local.google_resources
}

output "expanded_resources" {
  value = local.expanded_resources
}

output "hostnames" {
  value = module.google_module.hostnames
  description = "Host names of google compute instances"
}

output "internal_addresses" {
  value = module.google_module.internal_addresses
}

output "external_addresses" {
  value = module.google_module.external_addresses
}

provider "google" {
  project = var.google_project
  region  = local.google_region
  zone    = var.google_zone
}

module "sites" {
  source               = "./modules/site_types/"
  deployment_resources = module.deployment.deployment_resources
  prefix               = var.prefix
  os_image             = module.os_module.os_image
  ssh_user             = var.ssh_user
  ssh_public_key       = var.ssh_public_key
}

module "os_module" {
  source = "./modules/operating_system/"
  os_name = var.operating_system_name
  os_version = var.operating_system_version
}

module "deployment" {
  source = "./modules/deployment_size/"
  deployment_sites = var.deployment_sites
}

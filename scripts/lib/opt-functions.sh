# shellcheck shell=bash
opt_invalid() {
    bad_opt=$1
    shift
    "$@"
    echo "" >&2
    echo "Invalid Option: -${bad_opt}" >&2
    exit 1
}

opt_requires_arg() {
    bad_opt=$1
    shift
    "$@"
    echo "" >&2
    echo "ERROR: -${bad_opt} expects an argument" >&2
    exit 1
}

opt_missing_requirement() {
    bad_opt=$1
    shift
    "$@"
    echo "" >&2
    echo "MISSING: ${bad_opt}" >&2
    exit 1
}

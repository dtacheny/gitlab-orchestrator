# Run the Developer Toolkit

## Quickstart

Start and enter the developer toolkit immediately:

```sh
./scripts/dev-container/run-dev-container


The container runtime, image, and/or tag used can be overridden by environment variables:

- `CONTAINER_RUNTIME=podman`
- `CONTAINER_IMAGE=myimage`
- `CONTAINER_TAG=mytag`

If testing container updates locally before pushing to the upstream branch, run:

`CONTAINER_TAG=local ./scripts/dev-container/build-dev-container`

This will build an image with the tag `local`, and you can run it with:

`CONTAINER_TAG=local ./scripts/dev-container/run-dev-container`

## Advanced Toolkit Configuration

### Overview

The developer toolkit and its environment is controlled through environment
variables that may be set in [default](./README.md#default-secrets-locations) or
[user specified](./README.md#specifying-secrets-locations) locations.

**NOTE**: Any changes made to the developer toolkit configuration require it
to be re-built using [the steps outlined in the toolkit
quickstart](#quickstart). Toolkit settings, unlike deployment settings, will
never be reflected into the currently running container.

### Default Secrets Locations

The developer environment assumes that a directory named `secrets` will be
created at the top directory of the repository. This is where Google
credentials and the [**gitlab-cluster-rc**](../deploy/README.md#configuration-for-the-developer-container-environment) for deploy scripts should live.

The deploy tools will generate a default SSH keypair if none is provided.

### Specifying Secrets Locations

Configure the build and deploy scripts to look for secrets in an alternate
directory by setting `SECRETS_PATH` in `${HOME}/.gitlab-orchestrator-rc`.

```sh
SECRETS_PATH=/path/to/directory/containing/credentials/and/rcs
```

### Using Persistent SSH Keys

The automatically generated **SSH** key only persists in the container while
it is running. Long term and production deployments should use a persistent
**SSH** keypair.

Set either of the following in `${HOME}/.gitlab-orchestrator-rc`:

```sh
SSH_PATH=/path/to/directory/containing/ssh/keypairs
PRIVATE_KEY_PATHS=(/path/to/private_key1 /path/to/private_key2)
```

**NOTE:** Using `SSH_PATH` will make *all* contents of the specified
directory available to the developer's container. If this is not desirable,
then specify the path to the private key of one or many SSH keypairs using
`PRIVATE_KEY_PATHS` and each will be mounted into the container.

The container environment will look for keys specified using the
following priority order:

1. `PRIVATE_KEY_PATHS` set in `${HOME}/.gitlab-orchestrator-rc`
1. `-k` option to `run-dev-container`
    ```sh
    ./scripts/dev-container/run-dev-container -k "/path/to/ssh/private/key"
    ```
1. `SSH_PATH` set in `${HOME}/.gitlab-orchestrator-rc`
1. Dynamically generated ssh key

Each of the above options may also be set when the script runs, consult
`./scripts/dev-container/run-dev-container -h` for more information.

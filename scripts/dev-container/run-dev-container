#!/bin/bash

set -e

# Local Functions
#-------------------------------------------------------------------------------
add_keypath() {
    local new_key="${1}"
    local existing_keys=("${2}")

    for existing_key in "${existing_keys[@]}"; do
        if [ "${new_key}" = "${existing_key}" ]; then
            fail "NAME COLLISION: ${new_key} and ${existing_key}"
        else
            echo "${new_key}"
        fi
    done
}

add_mount() {
  local source=$1
  local target=$2

  container_mounts+=(--mount "type=bind,source=${source},target=/repo/${target}")

  echo "${source} is mounted to container at /repo/${target}"
}

usage() {
    name=$(basename "$0")
    echo "Usage:"
    echo "${name} [-c] DIRECTORY [-s] DIRECTORY [-k] PRIVATE_KEY"
    echo ""
    echo "Creates a container with all the tools needed to develop"
    echo "and test orchestrator playbooks."
    echo ""
    echo "    -h    Display help message"
    echo ""
    echo "    -c    Path to a directory that will contain cloud provider"
    echo "          credentials, GitLab Tokens, and user defined"
    echo "          configuration for the cluster"
    echo ""
    echo "    -s    Path to a directory containing SSH keypairs"
    echo ""
    echo "    -k    Path to the private key of an SSH keypair. Expects the"
    echo "          public key to be named KEY_NAME.pub where KEY_NAME is"
    echo "          the filename of the private key."
}

src_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../ >/dev/null 2>&1 && pwd )"
[ -z "${SRC_PATH}" ] && export SRC_PATH="${src_path}"

# Load User Configuration File
#-------------------------------------------------------------------------------
if [ -f "${HOME}/.gitlab-orchestrator-rc" ]; then
    # shellcheck source=/dev/null
    . "${HOME}/.gitlab-orchestrator-rc"
fi

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/src-paths.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

# Assemble SSH Keys defined in RC file
#-------------------------------------------------------------------------------
user_private_key_paths=()

for key in "${PRIVATE_KEY_PATHS[@]}"; do
    key_path="$(portable_readlink "${key}")"
    user_private_key_paths+=("$(add_keypath "${key_path}" "${user_private_key_paths[@]}")")
done

# User Options Capture
#-------------------------------------------------------------------------------
while getopts ":hc:s:k:" opt; do
    case "${opt}" in
        h )
            usage
            exit 1
            ;;
        c )
            directory_exists "${OPTARG}"
            user_secrets_path="$(portable_readlink "${OPTARG}")"
            ;;
        s )
            directory_exists "${OPTARG}"
            user_ssh_path="$(portable_readlink "${OPTARG}")"
            ;;
        k )
            file_exists "${OPTARG}"
            file_exists "${OPTARG}.pub"
            user_runtime_key_path="$(portable_readlink "${OPTARG}")"
            user_private_key_paths+=("$(add_keypath "${user_runtime_key_path}" "${user_private_key_paths[@]}")")
            ;;
        \? )
            opt_invalid "${OPTARG}" usage
            ;;
        : )
            opt_requires_arg "${OPTARG}" usage
            ;;
    esac
done

SECRETS_PATH="$(portable_readlink "${user_secrets_path:-${SECRETS_PATH}}")"
mkdir -p "${SECRETS_PATH}"

mkdir -p "${ARTIFACTS_PATH}"

declare -a container_mounts

# Add standard volume mounts
#TODO: look into refactoring. Would like to use associative arrays, but that
#  requires BASH 4. MacOS only ships with BASH 3.
add_mount "${ANSIBLE_PATH}" "$(basename "${ANSIBLE_PATH}")"
add_mount "${SECRETS_PATH}" "$(basename "${SECRETS_PATH}")"
add_mount "${TERRAFORM_PATH}" "$(basename "${TERRAFORM_PATH}")"
add_mount "${SCRIPTS_PATH}" "$(basename "${SCRIPTS_PATH}")"
add_mount "${ARTIFACTS_PATH}" "$(basename "${ARTIFACTS_PATH}")"

SSH_PATH="${user_ssh_path:-${SSH_PATH}}"

if [ -n "${SSH_PATH}" ]; then
    SSH_PATH="$(portable_readlink "${SSH_PATH}")"
    add_mount "${SSH_PATH}" "${HOST_SSH_KEY_DIR}"
fi

for key in "${user_private_key_paths[@]}"; do
    key_name="$(basename "${key}")"
    add_mount "${key}" "${MOUNTED_SSH_KEY_DIR}/${key_name}"
    add_mount "${key}".pub "${MOUNTED_SSH_KEY_DIR}/${key_name}.pub"
done

# Main Program
#-------------------------------------------------------------------------------
printf "\nStarting dev container..."
printf "\nEdit site configurations and secrets outside the container with a favorite editor.\n\nFile changes reflect immediately in the container for all files in the volume mounts listed above.\n"

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/dev-container/lib/default-config.sh"

"${CONTAINER_RUNTIME}" run --rm -it \
    "${container_mounts[@]}" \
    "${CONTAINER_IMAGE}:${CONTAINER_TAG}"

# shellcheck shell=bash

# Terraform Configuration Values
export TF_VAR_google_project="${GCLOUD_PROJECT}"
export TF_VAR_google_zone=${GCLOUD_ZONE}
export TF_VAR_prefix="${TERRAFORM_PREFIX}"
export TF_VAR_ssh_public_key="${SSH_PUBLIC_KEY}"
export TF_VAR_ssh_user="${ORCHESTRATION_USER}"

# Set Terraform to "automation" mode to adjust output for non-interactive mode
# Docs: https://www.terraform.io/docs/commands/environment-variables.html#tf_in_automation
export TF_IN_AUTOMATION='true'

# Functions
terraform_pretasks() {
    trap 'cd "${SRC_PATH}"' EXIT

    cd "${TERRAFORM_PATH}" || fail "Could not enter terraform directory"

    terraform init -input=false "${terraform_args[@]}"
}

inject_backend_type() {
  local type=$1

  printf "terraform {\n  backend \"${type}\" {}\n}" > "${TF_BACKEND_CONFIG_PATH}"
}

# Configure Terraform args based on backend type
case "${BACKEND}" in
  "gcs" )
    inject_backend_type "gcs"

    terraform_args=(
      -backend-config "bucket=${STATE_GCS_BUCKET}"
      -backend-config "prefix=${GCS_PREFIX}"
    )
    ;;
  "gitlab" )
    inject_backend_type "http"

    TF_BACKEND_ADDRESS="${GITLAB_API_V4_URL}/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_PROJECT_NAME}"
    terraform_args=(
      -backend-config="address=${TF_BACKEND_ADDRESS}"
      -backend-config="lock_address=${TF_BACKEND_ADDRESS}/lock"
      -backend-config="unlock_address=${TF_BACKEND_ADDRESS}/lock"
      -backend-config="username=${GITLAB_API_USERNAME}"
      -backend-config="password=${GITLAB_API_TOKEN}"
      -backend-config="lock_method=POST"
      -backend-config="unlock_method=DELETE"
      -backend-config="retry_wait_min=5"
    )
    ;;
  * )
    fail "Please set the 'BACKEND' variable to one of the following: gcs, gitlab"
    ;;
esac

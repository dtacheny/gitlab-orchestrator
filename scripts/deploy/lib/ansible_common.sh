# shellcheck shell=bash

# Environment Config
export ANSIBLE_FORCE_COLOR=true
export ANSIBLE_BECOME_METHOD="sudo"
export ANSIBLE_INVENTORY_FILE="${ARTIFACTS_PATH}/base.gcp.yml"
export ANSIBLE_EXTRA_VARS_FILE="${ARTIFACTS_PATH}/extra_vars.json"
export ANSIBLE_INV_DATA_FILE="${ARTIFACTS_PATH}/ansible_inventory.json"
export ANSIBLE_SSH_ARGS="-o UserKnownHostsFile=${SSH_KNOWN_HOSTS_FILE} -C -o ControlMaster=auto -o ControlPersist=60s"

# Load SSH Key for use by Ansible
eval "$(ssh-agent -s)"
echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null


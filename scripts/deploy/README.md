# Cluster Deployment Scripts

[[_TOC_]]

## Quick Start

The following assumes that [all the steps were already followed to enter the containerized developer environment](../dev-container/README.md). The `secrets` directory is mounted into the container, so they are most easily edited using a local text editor. The `deploy` scripts should only be run from inside the container.

If you run into any issues, refer to the [troubleshooting page](../../doc/troubleshooting.md).

### Configure the Cluster

A minimal setup only requires a **gitlab-cluster-rc.sh** file to configure passwords and a
[Google Credentials JSON file](https://www.terraform.io/docs/providers/google/guides/provider_reference.html#credentials).

**Note:** Adding, removing, or editing files in the `secrets` directory on
the local filesystem will be automatically reflected inside the container. Use a
favorite editor to work with these while preparing for a GitLab deployment.

1. Set up a bucket and directory in Google Storage Browser
1. Copy `secrets/gitlab-cluster-rc.sh.EXAMPLE` to `secrets/gitlab-cluster-rc.sh`
1. Alter the values in `secrets/gitlab-cluster-rc.sh` using the information
   from the created Google Storage Bucket. [Review the configuration-in-depth table for more information on each setting](#configuration-in-depth).
1. Download a Google Credentials JSON file and save it as
   `secrets/google-credentials.json`

### Unattended Installation

**Orchestrator** supports unattended installations if each variable from `gitlab-cluster-rc.sh` is present in the runtime environment. The names must be exact and are case-sensitive.

- GitLab continuous integration pipelines should create pipeline variables
- shell scripts should create **ENV** variables

### Run the Deployment

**Note:** The deployment scripts should always be run inside the developer
container. It contains the proper versions of all dependencies.

1. `provision-cluster` creates a cluster of GCP machines
1. `assemble-inventory` gathers all inventory information from the cluster
1. `run-orchestration` executes the orchestrate playbook
1. `decrypt-password` displays the initial password for the user named `root` in the GitLab web application interface
1. `deprovision-cluster` destroys the cluster of GCP machines

**NOTE**: It may take up to 30-40 seconds for Google to provision the nodes
appropriately. The `assemble-inventory` script obtains the remote keys for
`known_hosts`. If run too soon, it will cause Google Cloud to block the
deployment machine from connecting to the provisioned nodes.

## Deployment Support Scripts

- `fetch-cluster-logs` gathers logs from the cluster
- `validate-provisioner` will ensure the terraform scripts are OK
- `run-orchestration` with the `-p` flag allows arbitrary playbooks to run
    ```
    **NOTE:** Playbooks that produce artifacts can write them to the
    artifacts directory using the Ansible variable `artifacts_path`.
    ```
- `run-orchestration` with the `-t` flag displays the task execution times
- `run-orchestration` with the `-e` flag allows a JSON of values to be injected to an arbitrary playbook

## Configuration in Depth

### RC File Options

Create an **rc** file to control the developer container environment. This
will replicate the pipeline variables from continuous integration.

The deploy scripts look for `${SECRETS_PATH}/gitlab-cluster-rc.sh` by default.
Configure multiple **rc** files and pass them to the deploy scripts using
the `-c` option for more flexibility. An example file is provided in
`secrets/gitlab-cluster-rc.sh.EXAMPLE` containing the minimum setup.

| Variable | Description | Default | Example | Required |
| -------- | ----------- | ------- | ------- | -------- |
| `GOOGLE_JSON_FILE` | Name of the JSON file containing Google credentials. | google-credentials.json | my-user-creds.json | Yes |
| `GCLOUD_CREDENTIALS_JSON` | The full contents of a Google Cloud service account's credential file when downloaded in **JSON** format.| None | `{type: service_account, ...}` | No |
| `TERRAFORM_PREFIX` | Prefix labels attached to provisioned cluster resources. | *None* | my-unique-prefix | Yes |
| `STATE_GCS_BUCKET` | The bucket name in Google storage. | *None* | my-bucket | No |
| `GCS_PREFIX` | The directory name under the defined storage bucket. | *None* | my-state-storage-dir | No |
| `GITLAB_API_USERNAME` | The GitLab username to use for the [GitLab Terraform backend][gitlab-terraform-backend]. | None | gl-robot | No |
| `GITLAB_API_TOKEN` | The GitLab access token to use for the [GitLab Terraform backend][gitlab-terraform-backend]. | None | abcde12345 | No |
| `GITLAB_API_V4_URL` | The API URL for the target GitLab instance providing the [GitLab Terraform backend][gitlab-terraform-backend]. | None | https://gitlab.com/api/v4 | No |
| `GITLAB_PROJECT_NAME` | The project name in GitLab that will store the [GitLab Terraform backend][gitlab-terraform-backend]. | None | my-tf-backend | No |
| `GITLAB_PROJECT_ID` | The project ID in GitLab that will store the [GitLab Terraform backend][gitlab-terraform-backend]. | None | 123 | No |
| `GCLOUD_ZONE` | The name of the specific Google Cloud zone to deploy. | *None* | us-east1-b | Yes |
| `GCLOUD_PROJECT` | The project name in Google Cloud. | *None* | my-project-a | Only if not present in `GOOGLE_JSON_FILE` |
| `SSH_USER` | The name of the user Ansible will use to connect and configure nodes. | ansible | my-username | Yes |
| `SSH_KEY_NAME` | The file name of the private key for connecting to the cluster. | gitlab-orchestrator | No |
| `PACKAGE_URL` | URL pointing at a GitLab package to install. | *None* | https://path.to.my.package | No |
| `PACKAGE_REPOSITORY` | Opt to use either the Enterprise Edition or Community Edition in a repository. | *None* | `ee` or `ce` | No |
| `PACKAGE_REPOSITORY_URL`| URL to alternative packagecloud repository. | Points to the new repository defined in `PACKAGE_REPOSITORY`.|
| `GL_PRIVATE_TOKEN` | The GitLab Token that will be used to download packages from GitLab artifacts | *None* | my-token | No |
| `consul_pgbouncer_password` | Password for gitlab-consul user in postgres database | *None* | my-secret-pass | Yes |
| `application_pgbouncer_password` | Password for pgbouncer user in postgres database | *None* | my-secret-pass | Yes |
| `application_database_password` | General postgres database sql password | *None* | my-secret-pass | Yes |
| `ANSIBLE_BECOME_METHOD` | Method Ansible will use to escalate privileges. | sudo | `sudo` | No |
| `VAULT_PASSWORD_FILE` | [Ansible vault password file](https://docs.ansible.com/ansible/latest/user_guide/vault.html#providing-vault-passwords) used to encrypt/decrypt secrets | vault-password-file | my-ansible-password | [Check Details](#secret-encryption-at-runtime) |
| `GITLAB_LICENSE_FILE` | Name of file containing a GitLab license | GitLab.gitlab-license | my-gitlab-license | No |
| `INITIAL_GITLAB_ROOT_PASSWORD` | GitLab application root user password at initial installation. | Randomized | *None* | No |
| `TRACKING_DB_PASSWORD` | Geo Secondary site tracking database password at initial installation. | Randomized | *None* | No |
| `REPLICA_DB_PASSWORD` | Geo Secondary site replica database password at initial installation. | Randomized | *None* | No |
| `GRAFANA_ADMIN_PASSWORD` | Password for the default Grafana administrator account (`admin`). | Randomized | *None* | No |
| `GITALY_AUTH_TOKEN` | Gitaly authentication token. | Randomized | *None* | No |
| `GITLAB_SHELL_SECRET_TOKEN` | GitLab Shell secret token. | Randomized | *None* | No |
| `KEYSCAN_WAIT_SECONDS` | Number of seconds to wait before running `ssh-keyscan` in the `assemble-inventory` script. | 0 | 30 | No |
| `ARTIFACTS_PATH` | Path at which runtime-generated files will be stored. | `$SRC_PATH/artifacts` | `/my/artifacts/dir` | No |
| `TRACKING_DATABASE_PASSWORD` | Geo Secondary site tracking database password at initial installation. | Randomized | *None* | No |
| `REPLICA_DATABASE_PASSWORD` | Geo Secondary site replica database password at initial installation. | Randomized | *None* | No |
| `DB_KEY_BASE` | Sets the shared database field encryption key. | Randomized | *None* | No |

### Cluster Topology Options

Advanced configurations for features such as GitLab Geo may require the
deployment of multiple multi-node clusters. The terraform provisioner can
create this topology using a *JSON* definition file. The framework expects
the definition file to live in the `${SECRETS}` directory.

Set `GITLAB_CLUSTER_DESCRIPTION_JSON` to the name of the cluster definition
file as it appears in the `${SECRETS}` directory or name the file
`gitlab-cluster.json` for the framework to automatically discover it.

#### Configuring the Deployment Topology

The contents of a **JSON** definition has the form:

```json
{
    "deployment_sites": {
        "SITE_NAME": "DEPLOYMENT_SIZE"
    },
    "geo": {
        "enabled": "true|false",
        "primary": "PRIMARY_SITE_NAME",
        "secondaries": [
            "SECONDARY_SITE_NAME"
        ]
    }
}
```

1. `SITE_NAME` is a unique human-readable string. Each regional deployment of GitLab is
   known as a **site** and this will be its label in the GitLab Admin
   Interface. Avoid underscores `_` and dashes `-` until
   [issues with those characters are resolved](https://gitlab.com/gitlab-org/gitlab-orchestrator/-/issues/268).
1. `DEPLOYMENT_SIZE` is one of the values in the table below.

    |Option Value|Deployed Resources|
    |--|--|
    |`omnibus_pipeline`|<ul><li>1 application node</li><li>3 database nodes</li><li>3 consul nodes</li></ul>|
    |`geo_pipeline`|<ul><li>1 application node</li><li>1 database node</li></ul>|
    |`2k_users`|<ul><li>2 application nodes</li><li>1 database node</li><li>1 gitaly node</li><li>1 load-abalncer node</li><li>1 monitor node</li><li>1 redis node</li></ul>|
1. Define the relationship between Geo primary and secondary sites using the
   `SITE_NAME` values defined in the `deployment_sites` stanza.

#### Deployment Configuration Examples

Here are a few sample cluster types:

**NOTE:** The `secrets/examples` directory contains these and more examples.

- Set up a multi-node cluster
    ```json
    {
        "deployment_sites": {
            "main": "omnibus_pipeline"
        }
    }
    ```
- Set up a Geo multi-node cluster with a primary cluster named **main** and
  a geo secondary cluster named **geo1**
    ```json
    {
        "deployment_sites": {
            "main": "omnibus_pipeline",
            "geo1" : "geo_pipeline"
        },
        "geo": {
            "enabled": "true",
            "primary": "main",
            "secondaries": [
                "geo1"
            ]
        }
    }
    ```

### Secret Encryption at Runtime

The orchestrator encrypts secrets as necessary to maintain security. The
orchestrator uses **Ansible** under the hood and expects a file in the
`${SECRETS}` directory [formatted for `--vault-password-file`](https://docs.ansible.com/ansible/latest/user_guide/vault.html#providing-vault-passwords). No configuration is required if the default file name `vault-password-file` is used.

The `VAULT_PASSWORD_FILE` must exist in `${SECRETS}` if adding a license to
the GitLab deployment.

## Running the Deploy Scripts Outside the Developer Container

The following packages must be installed for these scripts to run:

- [Ruby 2.6.x or later](https://www.ruby-lang.org/en/)
- [jq](https://stedolan.github.io/jq/)
- [Ansible](https://www.ansible.com/)
- [Terraform](https://www.terraform.io/)

The developer container will always contain the proper versions of the tools
and provide a more seamless experience when using the orchestrator.

[gitlab-terraform-backend]: https://docs.gitlab.com/ee/user/infrastructure

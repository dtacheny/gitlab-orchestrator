#!/bin/bash

set -e

src_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../ >/dev/null 2>&1 && pwd )"
[ -z "${SRC_PATH}" ] && export SRC_PATH="${src_path}"

declare -a extra_vars_files

add_extra_vars_file() {
    vars_file_path=$1
    file_exists "${vars_file_path}"
    extra_vars_files+=(--extra-vars @"${vars_file_path}")
}

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/opt-functions.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

usage() {
    name=$(basename "$0")
    echo "Usage:"
    echo "${name} [-c] PATH [-p] PLAYBOOK"
    echo ""
    echo "Orchestrates the GitLab Cluster installation"
    echo ""
    echo "    -p    Override orchestration to run a playbook"
    echo ""
    echo "    -h    Display help message"
    echo ""
    echo "    -c    Path to a configuration file defining the settings for"
    echo "          orchestrating the GitLab Cluster in Google Cloud"
    echo ""
    echo "    -e    Path to a JSON file defining extra variables for usage"
    echo "          during an arbitrary playbook."
    echo ""
    echo "    -t    Display execution time of each task in the playbook."
}

while getopts ":hc:p:te:" opt; do
    case "${opt}" in
        h )
            usage
            exit 1
            ;;
        c )
            file_exists "${OPTARG}"
            GITLAB_CLUSTER_RC="$(portable_readlink "${OPTARG}")"
            export GITLAB_CLUSTER_RC
            ;;
        p )
            file_exists "${OPTARG}"
            TARGET_PLAYBOOK="$(portable_readlink "${OPTARG}")"
            export TARGET_PLAYBOOK
            ;;
        t )
            ANSIBLE_CALLBACK_WHITELIST=profile_tasks
            PROFILE_TASKS_SORT_ORDER=none
            PROFILE_TASKS_OUTPUT_LIMIT=all
            export ANSIBLE_CALLBACK_WHITELIST
            export PROFILE_TASKS_SORT_ORDER
            export PROFILE_TASKS_OUTPUT_LIMIT
            ;;
        e )
            file_exists "${OPTARG}"
            add_extra_vars_file "${OPTARG}"
            ;;
        \? )
            opt_invalid "${OPTARG}" usage
            ;;
        : )
            opt_requires_arg "${OPTARG}" usage
            ;;
    esac
done

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/orchestrator_env.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/ansible_common.sh"

add_extra_vars_file "${ANSIBLE_EXTRA_VARS_FILE}"

# main script
TARGET_PLAYBOOK="${TARGET_PLAYBOOK:-${ANSIBLE_PATH}/orchestrate.yml}"

if [ ! -f "${TARGET_PLAYBOOK}" ]; then
    usage
    exit 1
fi

ansible-playbook \
    -i "${ANSIBLE_INVENTORY_FILE}" \
    "${TARGET_PLAYBOOK}" \
    --vault-password-file "${VAULT_PASSWORD_PATH}" \
    "${extra_vars_files[@]}"

#!/bin/bash

set -e

src_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../ >/dev/null 2>&1 && pwd )"
[ -z "${SRC_PATH}" ] && export SRC_PATH="${src_path}"

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/opt-functions.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/lib/helpers.sh"

usage() {
    name=$(basename "$0")
    echo "Usage:"
    echo "${name} [-c] PATH"
    echo ""
    echo "Decrypts the initial root password"
    echo ""
    echo "    -h    Display help message"
    echo ""
    echo "    -c    Path to a configuration file defining the settings for"
    echo "          orchestrating the GitLab Cluster in Google Cloud"
}

while getopts ":hc:" opt; do
    case "${opt}" in
        h )
            usage
            exit 1
            ;;
        c )
            file_exists "${OPTARG}"
            GITLAB_CLUSTER_RC="$(portable_readlink "${OPTARG}")"
            export GITLAB_CLUSTER_RC
            ;;
        \? )
            opt_invalid "${OPTARG}" usage
            ;;
        : )
            opt_requires_arg "${OPTARG}" usage
            ;;
    esac
done

# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/orchestrator_env.sh"
# shellcheck source=/dev/null
. "${SRC_PATH}/scripts/deploy/lib/ansible_common.sh"

# main script
ansible-playbook \
    -i "${ANSIBLE_INVENTORY_FILE}" \
    "${ANSIBLE_PATH}/decrypt_password.yml" \
    --vault-password-file "${VAULT_PASSWORD_PATH}" \
    --extra-vars @"${ANSIBLE_EXTRA_VARS_FILE}"

package util

import (
	"os"
	"os/exec"
)

// RunShellCommand calls a command and writes to os.Std{out/err}.
func RunShellCommand(command string, args ...string) error {
	cmd := exec.Command(command, args...)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

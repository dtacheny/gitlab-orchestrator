package util

// SupportedValue returns true if a value is within a list of supported values.
// If the value is not in the supported list, it returns false.
func SupportedValue(supportedValues []string, value string) bool {
	for _, supported := range supportedValues {
		if value == supported {
			return true
		}
	}

	return false
}

package app

import (
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/commands"
)

const (
	appName  = "orchestrator"
	appUsage = "A project to orchestrate installations of GitLab for internal testing, customer sites, and any other applicable use cases."
	appHelp  = `
{{.Name}} - {{.Usage}}

Usage: {{.Name}} {{if .VisibleFlags}}[global options]{{end}}{{if .Commands}} command [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}

The available commands for execution are listed below.

Commands:
{{range .Commands}}{{if not .HideHelp}}{{ "\t"}}{{join .Names ", "}}{{ "\t"}}{{.Usage}}{{ "\n" }}{{end}}{{end}}

Global options:
{{- range .VisibleFlags}}
{{ "\t"}}{{.}}
{{end}}
`
	commandHelp = `
{{.Name}} - {{.Usage}}

Usage: orchestrator {{.Name}} {{if .VisibleFlags}}[command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}

Options:
{{- range .VisibleFlags}}
{{ "\t"}}{{.}}
{{end}}
`
)

// New creates cli.App for the Orchestrator CLI
func New() *cli.App {
	cli.AppHelpTemplate = appHelp
	cli.CommandHelpTemplate = commandHelp
	return &cli.App{
		Name:  appName,
		Usage: appUsage,
		Commands: []*cli.Command{
			commands.Init(),
		},
	}
}

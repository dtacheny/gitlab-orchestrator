package commands

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/util"
)

const (
	defaultImage   = "registry.gitlab.com/gitlab-org/gitlab-orchestrator"
	defaultTag     = "latest" // TODO: should be latest release compatible with this CLI
	defaultRuntime = "docker"
)

// getSupportedRuntimes returns the list of supported runtimes.
// Defined as a function because Go does not allow complex types to be constant.
func getSupportedRuntimes() []string {
	return []string{"docker", "podman"}
}

// Init downloads dependencies on the first run
func Init() *cli.Command {
	return &cli.Command{
		Name:  "init",
		Usage: "Install dependencies on first run.",
		Action: func(ctx *cli.Context) error {
			return manageDependencies(ctx)
		},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:     "auto-update-dependencies",
				Usage:    "Enable automatic updates of runtime container based on image and tag settings.",
				Required: false,
				Aliases:  []string{"u"},
				EnvVars:  []string{"AUTO_UPDATE_DEPENDENCIES"},
				Value:    false,
			},
			&cli.StringFlag{
				Name:     "image-file",
				Usage:    "Provide runtime container image tarball.",
				Required: false,
				Aliases:  []string{"f"},
				EnvVars:  []string{"IMAGE_FILE"},
				Value:    "",
			},
			&cli.StringFlag{
				Name:     "runtime",
				Usage:    fmt.Sprintf("Use %s container runtime.", strings.Join(getSupportedRuntimes(), " or ")),
				Required: false,
				Aliases:  []string{"r"},
				EnvVars:  []string{"RUNTIME"},
				Value:    defaultRuntime,
			},
			&cli.StringFlag{
				Name:     "image",
				Usage:    "Select alternate runtime container.",
				Required: false,
				Aliases:  []string{"i"},
				EnvVars:  []string{"IMAGE"},
				Value:    defaultImage,
			},
			&cli.StringFlag{
				Name:     "tag",
				Usage:    "Select runtime container tag.",
				Required: false,
				Aliases:  []string{"t"},
				EnvVars:  []string{"TAG"},
				Value:    defaultTag,
			},
		},
	}
}

func manageDependencies(ctx *cli.Context) error {
	logInfo := log.New(os.Stderr, "orchestrator | [ INFO ] ", log.Ldate|log.Ltime)
	logError := log.New(os.Stderr, "orchestrator | [ ERROR ] ", log.Ldate|log.Ltime)

	supportedRuntimes := getSupportedRuntimes()
	if !util.SupportedValue(supportedRuntimes, ctx.String("runtime")) {
		logError.Printf("'%s' is not a supported container runtime.", ctx.String("runtime"))
		cli.ShowCommandHelpAndExit(ctx, ctx.Command.Name, 1)
	}

	logInfo.Printf("Container runtime set to %s.", ctx.String("runtime"))

	if ctx.Bool("auto-update-dependencies") {
		logInfo.Println("Automatically updating dependencies...")

		err := downloadDependencies(ctx)
		if err != nil {
			return fmt.Errorf("failed to download dependencies: %w", err)
		}

		logInfo.Println("Successfully downloaded dependencies.")
	} else {
		logInfo.Println("Flag '--auto-update-dependencies' is false. Attempting to load custom image...")

		// Ensure 'image-file' flag is provided
		if ctx.String("image-file") == "" {
			logError.Println("Flag '--image-file' not provided for command 'init'.")
			cli.ShowCommandHelpAndExit(ctx, ctx.Command.Name, 1)
		}

		err := loadImage(ctx)
		if err != nil {
			return fmt.Errorf("failed to load image: %w", err)
		}

		logInfo.Println("Successfully loaded image.")
	}

	return nil
}

func downloadDependencies(ctx *cli.Context) error {
	// Construct full container path
	command := ctx.String("runtime")
	image := ctx.String("image")
	tag := ctx.String("tag")
	path := fmt.Sprintf("%s:%s", image, tag)
	args := []string{"pull", path}

	// Attempt to pull image from registry
	if err := util.RunShellCommand(command, args...); err != nil {
		return fmt.Errorf("failed to pull image from registry: %w", err)
	}

	return nil
}

func loadImage(ctx *cli.Context) error {
	// Construct 'load' command
	command := ctx.String("runtime")
	file := ctx.String("image-file")
	args := []string{"load", "-i", file}

	// Attempt to load image
	if err := util.RunShellCommand(command, args...); err != nil {
		return fmt.Errorf("failed to load custom image: %w", err)
	}

	return nil
}

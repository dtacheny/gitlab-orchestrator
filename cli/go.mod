module gitlab.com/gitlab-org/gitlab-orchestrator/cli

go 1.15

require github.com/urfave/cli/v2 v2.2.0

PROJECT_NAME := orchestrator
BINARY_PATH  ?= /usr/local/bin
LD_FLAGS     ?= '-s -w'

.PHONY: build install deps clean run help

build: clean ## Runs 'go build' with ldflags and places binary in project root directory
	@go build -ldflags $(LD_FLAGS) -o ./$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

install: build ## Copies the binary to directory in $PATH
	@cp ./$(PROJECT_NAME) $(BINARY_PATH)

deps: ## Update dependencies
	@go mod tidy

clean: ## Removes generated binary
	@rm -f $(PROJECT_NAME)
	@rm -f $(BINARY_PATH)/$(PROJECT_NAME)

run: ## Runs the CLI
	@go run cmd/$(PROJECT_NAME)/main.go

# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Prints help information
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

# Inspiration from https://gitlab.com/gitlab-org/release-cli

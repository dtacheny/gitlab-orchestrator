package main

import (
	"log"
	"os"

	"gitlab.com/gitlab-org/gitlab-orchestrator/cli/internal/app"
)

func main() {
	if err := app.New().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

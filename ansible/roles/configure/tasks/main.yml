---
- name: Assemble gitlab.rb
  assemble:
    src: "{{ gitlab_conf_fragments_directory }}"
    dest: /etc/gitlab/gitlab.rb
    mode: 0600
  become: yes
  notify:
    - reconfigure gitlab

- name: restart database if needed
  command: gitlab-ctl restart postgresql
  when:
    - restart_database
    - "'database' in group_names"

# if the playbooks somehow failed, retain idempotence via extra check
- name: verify services run
  command: gitlab-ctl status
  become: yes
  register: services_test
  failed_when: false
  changed_when: (not services_test.stdout) or (services_test.rc != 0)
  notify: reset gitlab

- name: flush handlers
  meta: flush_handlers

- name: Create .pgpass file
  command: /opt/gitlab/bin/gitlab-ctl write-pgpass --host 127.0.0.1 --database pgbouncer --hostuser gitlab-consul
  args:
    stdin: "{{ application_pgbouncer_password }}\n{{ application_pgbouncer_password }}\n"
    creates: /var/opt/gitlab/consul/.pgpass
  become: yes
  notify:
    - reset gitlab
    - restart consul
  when:
    - "'app' in group_names"
    - consul_enabled

- name: flush handlers
  meta: flush_handlers

- name: apply migrations
  environment:
    GITLAB_ROOT_PASSWORD: "{{ gitlab_root_password }}"
  command: /opt/gitlab/bin/gitlab-rake gitlab:db:configure
  tags:
    - migrate
  become: yes
  when:
    - run_migrations
    - "'app' in group_names"
    - ansible_hostname == primary_app_server_hostname

- name: apply geo migrations
  script: scripts/geo-db-migrate
  tags:
    - migrate
  become: yes
  when:
    - run_geo_migrations
    - "'app' in group_names"

- name: deal with database registration
  block:
    - name: Count the number of nodes in the visible repmgr cluster
      command: gitlab-psql -t -d gitlab_repmgr -c 'select count(*) from repmgr_gitlab_cluster.repl_nodes;'
      args:
        chdir: /tmp
      register: node_count
      become: yes
      changed_when: False

    - name: Count the number of nodes we expect to see
      set_fact:
          current_count: "{{ groups['database'] | intersect(groups[cluster_name]) | length }}"
          node_count: "{{ node_count.stdout | int }}"

    - name: Do we see all the nodes
      fail:
        msg="Not enough nodes, and expected_master is not defined"
      when:
        - node_count != current_count
        - expected_master is not defined

    - name: Register with the expected master
      command: gitlab-ctl repmgr standby setup {{ expected_master }} -w
      when:
        - node_count != current_count
        - expected_master is defined
        - ansible_hostname != expected_master
      become: yes
  when:
    - "'database' in group_names"
    - consul_enabled

- name: Manage GitLab license
  block:
    - name: Check current license
      command: |
        gitlab-rails runner 'puts License.exists?(data: "{{ license_data }}")'
      register: license_check
      changed_when: license_check.stdout == "false"

    - name: Install license
      command: |
        gitlab-rails runner 'License.create(data: "{{ license_data }}")'
      when: license_check.stdout == "false"
  when:
    - license_data is defined
    - "'app' in group_names"
    - (primary_cluster or not geo_config.user_enabled)
  become: yes
  tags:
    - license

- name: Geo Primary Tasks
  block:
    - name: Become the primary node
      command: gitlab-ctl set-geo-primary-node
      become: yes

    - name: Update application settings
      command: "gitlab-rails runner 'ApplicationSetting.last.update(authorized_keys_enabled: false, hashed_storage_enabled: true)'"
  when:
    - geo_config.user_enabled
    - primary_cluster
    - "'app' in group_names"

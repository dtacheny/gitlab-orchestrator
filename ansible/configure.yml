---
- hosts: consul
  roles:
      - { role: configure , become: yes}

- hosts: database
  roles:
      - { role: configure , become: yes}

- hosts: gitaly
  roles:
      - { role: configure , become: yes}

- hosts: redis
  roles:
      - { role: configure , become: yes}

- hosts: app
  roles:
      - { role: configure , become: yes}

- hosts: monitor
  roles:
      - { role: configure , become: yes}

- hosts: app
  tasks:
  - name: Register the Monitor node
    become: yes
    command: |
      gitlab-rails runner "ApplicationSetting.update_all(grafana_enabled: true, grafana_url: 'http://{{ monitor_node_external_ip }}/-/grafana')"
    when:
      - ansible_hostname == primary_app_server_hostname
      - external_monitor_enabled

- hosts: app:&primary_geo_site
  become: yes
  tasks:
  - name: primary site app node tasks
    block:
    - name: obtain primary app node secrets
      fetch:
        src: /etc/gitlab/gitlab-secrets.json
        dest: "{{ fetched_gitlab_secrets }}"
        flat: yes
    - name: obtain primary cluster ssh
      synchronize:
        src: /etc/ssh/ssh_host_*
        dest: "{{ fetched_ssh_keys }}"
        mode: pull
    when:
      - geo_config.user_enabled

- hosts: app:!primary_geo_site
  become: yes
  tasks:
  - name: secondary site app node tasks
    block:
    - name: check for SSH keys backup file
      stat:
        path: /etc/ssh/old_ssh_host_keys_gitlab.tar.gz
      register: ssh_key_backup_data

    - name: archive original ssh keys
      archive:
        path: /etc/ssh/ssh_host*
        dest: /etc/ssh/old_ssh_host_keys_gitlab.tar.gz
        format: gz
        mode: 0600
      when:
        - not ssh_key_backup_data.stat.exists

    - name: install ssh keys from primary app server
      synchronize:
        src: "{{ fetched_ssh_keys }}"
        dest: /etc/ssh/
        mode: push
      register: secondary_ssh_keys

    - name: Reset SSH Known Hosts if SSH Keys Changed
      block:
      - name: make sure assembly directory exists
        file:
          path: "{{ ssh_known_hosts_path }}_assembly"
          state: directory
          mode: 0700
      - name: identify primary app server entries
        shell:
          cmd: grep "{{ primary_app_server_ip }}" "{{ ssh_known_hosts_path }}" > "{{ ssh_known_hosts_path }}_assembly/{{ ansible_host }}.entries"
          creates: "{{ ssh_known_hosts_path }}_assembly/{{ ansible_host }}"
      - name: identify other server entries
        shell:
          cmd: grep -v "{{ ansible_host }}" "{{ ssh_known_hosts_path }}" > "{{ ssh_known_hosts_path }}_assembly/other_hosts.entries"
          creates: "{{ ssh_known_hosts_path }}_assembly/{{ ansible_host }}"
      - name: reconfigure export for geo host
        replace:
          path: "{{ ssh_known_hosts_path }}_assembly/{{ ansible_host }}.entries"
          regexp: "{{ primary_app_server_ip }}"
          replace: "{{ ansible_host }}"
      - name: assemble new known_hosts file
        assemble:
          src: "{{ ssh_known_hosts_path }}_assembly"
          dest: "{{ ssh_known_hosts_path }}.new"
      - name: install new known_hosts file
        copy:
          src: "{{ ssh_known_hosts_path }}.new"
          dest: "{{ ssh_known_hosts_path }}"
          owner: root
          group: root
          mode: 0644
          backup: yes
      delegate_to: 127.0.0.1
      when: secondary_ssh_keys.changed
    when:
      - geo_config.user_enabled

- hosts: all
  become: yes
  tasks:
  - name: universal geo site tasks
    block:
    - name: install common gitlab-secrets.json
      copy:
        backup: yes
        dest: /etc/gitlab/gitlab-secrets.json
        src: "{{ shared_gitlab_secrets }}"
        owner: root
        group: root
        mode: 0600
      notify:
        - reconfigure gitlab
        - restart postgresql
    when:
      - geo_config.user_enabled
  handlers:
  - name: reconfigure gitlab
    command: /opt/gitlab/bin/gitlab-ctl reconfigure
  - name: restart postgresql
    command: /opt/gitlab/bin/gitlab-ctl restart postgresql
    when:
      - "'database' in group_names"

- hosts: database:!primary_geo_site
  become: yes
  tasks:
  - name: secondary geo site database tasks
    block:
    - name: start replication
      expect:
        command: "gitlab-ctl replicate-geo-database --slot-name={{ cluster_name }} --host={{ replication_source_ip }} --no-wait"
        responses:
          (?i)password: "{{ replica_database_password }}"
        timeout: 90
      notify:
        - reconfigure gitlab
    when:
      - geo_config.user_enabled
  handlers:
  - name: reconfigure gitlab
    command: /opt/gitlab/bin/gitlab-ctl reconfigure

- hosts: database
  become: yes
  tasks:
  - name: universal geo site database tasks
    block:
    - name: determine postgresql user home directory
      getent:
        database: passwd
        key: "{{ postgresql_username }}"
    - name: set postgresql_user_home
      set_fact:
        postgresql_user_home: "{{ getent_passwd[postgresql_username][entity_home_directory_id] }}"
    - name: obtain SSL cert from primary
      block:
      - name: obtain database ssl certificates
        fetch:
          src: "{{ postgresql_user_home }}/data/server.crt"
          dest: "{{ fetched_db_ssl }}"
          flat: yes
    when:
      - geo_config.user_enabled

- hosts: database:!primary_geo_site
  become: yes
  tasks:
  - name: secondary geo site database tasks
    block:
    - name: create the .postgresql directory
      file:
        path: "{{ postgresql_user_home }}/.postgresql/public_certs"
        state: directory
        owner: gitlab-psql
        group: gitlab-psql
        mode: 0500
    - name: install public ssl certificate from primary cluster replication source
      copy:
        dest: "{{ postgresql_user_home }}/.postgresql/public_certs/{{ replication_source_node_name }}.crt"
        src: "{{ replication_source_node_certificate }}"
        owner: gitlab-psql
        group: gitlab-psql
        mode: 0400
    - name: install public ssl certificate from secondary cluster replication target
      copy:
        dest: "{{ postgresql_user_home }}/.postgresql/public_certs/{{ replication_target_node }}.crt"
        src: "{{ replication_target_node_certificate }}"
        owner: gitlab-psql
        group: gitlab-psql
        mode: 0400
    - name: install concatenated ssl certificate
      assemble:
        src: "{{ postgresql_user_home }}/.postgresql/public_certs"
        dest: "{{ postgresql_user_home }}/.postgresql/root.crt"
        owner: gitlab-psql
        group: gitlab-psql
        mode: 0400
    when:
      - geo_config.user_enabled


- hosts: app:&primary_geo_site
  become: yes
  tasks:
  - name: primary geo site app node tasks
    block:
    - name: generate secondary map fact
      set_fact:
        secondary_map: "{{ { 'ip': groups['app'] | intersect(groups[item]) | map('extract', hostvars, 'ansible_host') | list | first, 'name': item } }}"
      loop: "{{ secondary_clusters }}"
      register: secondary_map_result
    - name: create secondary looping fact
      set_fact:
        secondary_data: "{{ secondary_map_result.results | map(attribute='ansible_facts.secondary_map')  | list }}"
    - name: join secondary clusters to primary cluster
      command: |
        gitlab-rails runner 'GeoNode.create!(name: "{{ item.name }}", url: "http://{{ item.ip }}", primary: false)'
      register: join_command
      changed_when: join_command.rc == 0
      ignore_errors: yes
      loop: "{{ secondary_data }}"
      loop_control:
        label: "Adding {{ item.name }} as a secondary cluster"
    when:
      - geo_config.user_enabled
